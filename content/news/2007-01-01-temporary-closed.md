---
title: "Временное закрытие аэропорта"
date: 2007-01-01

slug: "temporary-closed"
---

В связи с указом Транспортного департамента Сектора Z, мы вынуждены
сообщить о закрытии аэропорта с 2007-01-01 до 2007-01-15 из-за
возможного риска наводнений. Приносим свои извинения за возможные
неудобства.
