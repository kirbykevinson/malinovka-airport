---
title: "Как добраться"
weight: 1

type: "directions"
---

<iframe src="https://www.openstreetmap.org/export/embed.html?marker=0.0,180.0&layer=cyclemap" class="map"></iframe>

Добраться до нашего аэропорта можно из любой точки Сектора Z при
помощи сервиса HyperZoom 2. Для этого вам необходимо найти ближайшую
станцию в вашем городе и оплатить поездку на поде. Сервис работает по
следующему расписанию:
