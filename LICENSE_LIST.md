## SIL Open Font License (`LICENSE_OFL`)

* `static/fonts/source-sans-pro.woff2` -
  [source](https://github.com/adobe-fonts/source-sans-pro/blob/4bdf42c690a214a0f69410d71a6b889c5c4a695f/WOFF2/OTF/SourceSansPro-Regular.otf.woff2)
  > Copyright 2010-2019 Adobe (http://www.adobe.com/), with Reserved
  > Font Name 'Source'. All Rights Reserved. Source is a trademark of
  > Adobe in the United States and/or other countries.
* All other files
  > Copyright 2021 Kirby Kevinson
