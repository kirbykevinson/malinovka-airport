.PHONY: gitlab build rsync archive

gitlab:
	git push

build:
	hugo

rsync: build
	rsync -rtvzP public/ root@bruh.ltd:/var/www/mvk

archive: build
	zip -r $$(date +"%F")-mvk.bruh.ltd.zip *
